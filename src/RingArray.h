/**
 * @file RingArray.h
 *
 * @brief A ring array, using next/prev flags for mid-index removal
 *
 * RingArrays are fixed-size arrays of values which wrap in a similar
 * manner to a ring buffer.  Prepending values and removing values from
 * middle indices is faster than in a simple array list because no memory copy
 * is needed.  Iterating over the values is faster than in a simple linked list
 * because values are kept in a (relatively) contiguous chunk of memory.
 * In the case where a value is removed from a middle index, the next idx value
 * on the previous entry is changed to point to the following entry.
 *
 * The RingArray is intended to provide fast iteration, appends, prepends,
 * and removes, while avoiding the memory indirection of linked lists.  Random
 * access is not possible since the actual array index of a given entry is not
 * known without iterating over the whole array.
 */

#ifndef DATASTRUCT_RINGARRAY_H
#define DATASTRUCT_RINGARRAY_H

#include <stdbool.h>
#include <stdint.h>

struct RingArray {
    /** Raw data of the array */
    char *data;

    /** Index of first entry in the array */
    size_t head;

    /** Index one past last entry in the array (mod capacity) */
    size_t tail;

    /** Count of number of active entries in array (excluding inactives) */
    size_t length;

    /** True if the array has no holes between start and end */
    bool is_packed;

    /** Size of a single data entry of array (including next & prev values) */
    size_t _entry_size;

    /** Size of values in array entries */
    size_t _val_size;

    /** Entry capacity of array including holes from middle-index removes */
    size_t _capacity;

    /** Load factor used to automatically grow array capacity
      * Resizes will be triggered when array wraps and
      * length / _capacity >= _load_factor */
    float _load_factor;
};

/**
 * Allocate a new RingArray for use.
 *
 * @param val_size      The size of a single value in the array
 * @param capacity      The initial capacity for the RingArray. Will be rounded
 *                      up to nearest power of 2 if necessary.
 * @param load_factor   The length/capacity ratio at which the RingArray
 *                      capacity will be resized to the next highest power of 2.
 * @return              A pointer to a new RingArray, or NULL if it was not
 *                      possible to allocate the memory.
 */
extern struct RingArray *ring_array_create(const size_t val_size,
                                           const size_t capacity,
                                           float load_factor);

/**
 * Destroy a RingArray and free back the memory it uses.
 *
 * @param RingArray     The RingArray to free.
 */
extern void ring_array_destroy(struct RingArray *ring_array);

/**
 * Prepend a value to the beginning of a RingArray
 *
 * @param ring_array   The RingArray.
 * @param v            The value to prepend.
 * @return             true if the request was successful, false if it was
 *                     not (likely because of a memory allocation failure)
 */
extern bool ring_array_push_front(struct RingArray *ring_array, const void *v);

/**
 * Append a value to the end of a RingArray.
 *
 * @param ring_array    The RingArray.
 * @param v             The value to append.
 * @return              true if the request was successful, false if it was
 *                      not possible to allocate more memory for the new entry.
 */
extern bool ring_array_push_back(struct RingArray *ring_array, const void *v);

/**
 * Remove the entry at the specified location in a RingArray by setting to
 * inactive.  If the entry is at start or end index, reposition that index
 * appropriately.
 *
 * @param ring_array    The RingArray.
 * @param idx           The array index to remove
 * @return              True if an active entry was removed, zero otherwise
 */
extern void ring_array_remove(struct RingArray *ring_array, const size_t idx);

/**
 * Remove a range of entries starting at the specified location in a RingArray.
 *
 * @param ring_array    The RingArray.
 * @param idx           The array index to begin removes
 * @param length        The number of entries to attempt to remove
 */
extern void ring_array_remove_range(struct RingArray *ring_array,
                                    const size_t idx, size_t length);

/**
 * Remove all entries from a RingArray.
 *
 * @param ring_array    The RingArray.
 */
extern void ring_array_clear(struct RingArray *ring_array);

#endif
