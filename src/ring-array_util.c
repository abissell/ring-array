#include "ring-array_util.h"

size_t next_power_of_2(size_t i)
{
    --i;
    i |= i >> 1;
    i |= i >> 2;
    i |= i >> 4;
    i |= i >> 8;
    i |= i >> 16;
    ++i;

    return i;
}
