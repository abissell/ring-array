#include <stddef.h>
#include <stdio.h>

int main(void) {
    size_t start = 5;
    size_t end = 3;
    size_t capacity = 8;

    size_t diff = (end - start) & (capacity - 1);
    printf("diff = %d\n", diff);

    size_t tail = 0;
    size_t last_entry = (tail - 1) & (capacity - 1);
    printf("last_entry = %d\n", last_entry);
}
