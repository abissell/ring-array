#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stddef.h>
#include <lber.h>
#include "ring-array_util.h"
#include "RingArray.h"

// Definition of long int constants
#define RING_ARY_END (SIZE_MAX)
#define RING_ARY_BEGIN (SIZE_MAX - 1)
#define MIN_CAPACITY (8)
#define MAX_CAPACITY (SIZE_MAX / 2)

// Definition of float constants
#define MIN_LOAD_FACTOR (0.3)
#define MAX_LOAD_FACTOR (0.9)

// Finds idx at which to align existing vals before prepending
// a new val, given a cur_length and intended new_capacity
static inline size_t prepend_align_idx(size_t cur_length,
                                       size_t new_capacity)
{
    const size_t new_length = cur_length + 1;
    assert(new_length < new_capacity);

    const size_t after_prepend_start = (new_capacity - new_length) / 2;
    return after_prepend_start + 1;
}

// Finds idx at which to align existing vals before appending
// a new val, given a cur_length and intended new_capacity
static inline size_t append_align_idx(size_t cur_length,
                                      size_t new_capacity)
{
    const size_t new_length = cur_length + 1;
    assert(new_length < new_capacity);

    return (new_capacity - new_length) / 2;
}

static inline bool needs_resize(struct RingArray *ring_array, size_t new_vals)
{
    const size_t new_length = ring_array->length + new_vals;
    const float new_load = (float)new_length / (float)ring_array->_capacity;
    return new_load >= ring_array->_load_factor;
}

// Increments the index by n entries, wrapping around to 0
static inline size_t incr_idx(size_t idx, size_t n, size_t capacity)
{
    assert(idx <= capacity);
    return (idx + n) & (capacity - 1);
}

// Decrements the index by n entries, wrapping around to capacity-1
static inline size_t decr_idx(size_t idx, size_t n, size_t capacity)
{
    assert(idx <= capacity);
    return (idx - n) & (capacity - 1);
}

// Obtains default head idx for array of given capacity (leaving prepend room)
static inline size_t default_head(size_t capacity)
{
    return capacity / 4;
}

// Get difference between start and end in array with size == capacity
static inline size_t idx_diff(size_t start, size_t end, size_t capacity)
{
    assert(start < capacity);
    assert(end < capacity);

    return (end - start) & (capacity - 1);
}

enum DefragRes {
    DEFRAG_FAILED,
    RE_INDEXED, // Re-index may be to same head value as before
    RE_INDEXED_AND_RESIZED
};

// Defragments the array by:
// - Copying all active cells to a contiguous region starting at ->head, then
// - Copying that active region to begin at start_idx, resizing the backing
//   value array if necessary.
static enum DefragRes defragment(struct RingArray *const ring_array,
                                 const size_t start_idx,
                                 const bool force_resize);

enum WrapHandle {
    HANDLE_FAILED,
    DEFRAGMENTED,
    RESIZED
};

// Handles a wrapped ring array (which did NOT need a resize based on
// load_factor) by:
// - Running defragment() to pack the entries and possibly resize
// - Resizing the array if the first step did not create sufficient room
static enum WrapHandle handle_wrapped(struct RingArray *const ring_array,
                                      const size_t start_idx);

static inline char *get_raw_entry_at_idx(char *data, const size_t idx,
        const size_t entry_size)
{
    return data + (idx * entry_size);
}

static inline char *get_entry_at_idx(struct RingArray *const ring_array,
                                     const size_t idx)
{
    return get_raw_entry_at_idx(ring_array->data, idx, ring_array->_entry_size);
}

static inline void set_next_idx_on_raw_entry(char *entry, const size_t next)
{
    size_t *entry_idx_ptr = (size_t *) entry;
    *entry_idx_ptr = next;
}

static inline void set_next_idx_on_entry(struct RingArray *const ring_array,
                                         const size_t entry_idx,
                                         const size_t next)
{
    char *entry = get_entry_at_idx(ring_array, entry_idx);
    set_next_idx_on_raw_entry(entry, next);
}

static inline void set_prev_idx_on_raw_entry(char *entry, const size_t prev)
{
    size_t *entry_idx_ptr = (size_t *) entry;
    ++entry_idx_ptr;
    *entry_idx_ptr = prev;
}

static inline void set_prev_idx_on_entry(struct RingArray *const ring_array,
                                         const size_t entry_idx,
                                         const size_t prev)
{
    char *entry = get_entry_at_idx(ring_array, entry_idx);
    set_prev_idx_on_raw_entry(entry, prev);
}

static inline void set_idxs_on_entry(struct RingArray *const ring_array,
                                     const size_t entry_idx,
                                     const size_t next, const size_t prev)
{
    set_next_idx_on_entry(ring_array, entry_idx, next);
    set_prev_idx_on_entry(ring_array, entry_idx, prev);
}

static inline void copy_val_to_entry(struct RingArray *const ring_array,
                                     const size_t entry_idx, const void *v)
{
    char *entry = get_entry_at_idx(ring_array, entry_idx);
    entry += (2*sizeof(size_t));
    memcpy(entry, v, ring_array->_val_size);
};

static inline size_t get_next_idx_on_entry(const char *entry)
{
    size_t *entry_idx_ptr = (size_t *) entry;
    return *entry_idx_ptr;
}

static inline size_t get_prev_idx_on_entry(const char *entry)
{
    size_t *entry_idx_ptr = (size_t *) entry;
    ++entry_idx_ptr;
    return *entry_idx_ptr;
}

static void apply_idx_delta(char *const data, char *const last_entry_to_update,
                            const size_t first_entry_idx,
                            const size_t entry_size, const size_t first_prev,
                            const ptrdiff_t delta)
{
    char *first_entry = get_raw_entry_at_idx(data, first_entry_idx, entry_size);

    // update first n-1 entries
    char *entry = first_entry;
    size_t prev = first_prev;
    while (entry != last_entry_to_update) {
        set_prev_idx_on_raw_entry(entry, prev);
        size_t new_next = get_next_idx_on_entry(entry) + delta;
        set_next_idx_on_raw_entry(entry, new_next);
        entry = get_raw_entry_at_idx(data, new_next, entry_size);
        prev = get_prev_idx_on_entry(entry) + delta;
    }

    // set next and prev on last entry to be updated
    size_t next = get_next_idx_on_entry(entry);
    if (next != RING_ARY_END) {
        set_next_idx_on_raw_entry(entry, next + delta);
    }

    if (entry != first_entry) {
        prev = get_prev_idx_on_entry(entry);
        set_prev_idx_on_raw_entry(entry, prev + delta);
    } else { // degenerate case of only 1 entry to update
        set_prev_idx_on_raw_entry(entry, first_prev);
    }
}

// Allocates memory for an array of capacity entries of size entry_size
static inline char *alloc_data(const size_t capacity, const size_t entry_size);

struct RingArray *ring_array_create(const size_t val_size, size_t capacity,
                                    float load_factor)
{
    if (val_size == 0) {
        return NULL;
    }

    const size_t orig_capacity = capacity;
    capacity = next_power_of_2(capacity);
    if (orig_capacity != capacity) {
        printf("[WARN] %s adjusted capacity from orig=%zu to "
               "next-power-of-2=%zu\n",
               __func__, orig_capacity, capacity);
    }

    // If the capacity is not well specified, use a sensible default
    if (capacity < MIN_CAPACITY) {
        printf("[WARN] %s found capacity=%zu <= MIN_CAPACITY=%zu, reset to "
               "MIN_CAPACITY\n",
               __func__, capacity, MIN_CAPACITY);
        capacity = MIN_CAPACITY;
    }

    const size_t entry_size = 2*sizeof(size_t) + val_size;
    if ((capacity * entry_size) > MAX_CAPACITY) {
        printf("[ERROR] %s found capacity=%zu > MAX_CAPACITY=%zu, returning "
               "NULL\n", __func__,
               capacity, MAX_CAPACITY);
        return NULL;
    }

    // Allocate new RingArray. There are initially no entries
    struct RingArray *new_ring_array =
            (struct RingArray *)malloc(sizeof(struct RingArray));

    if (new_ring_array == NULL) {
        return NULL;
    }

    // If the load_factor is not well specified, use a sensible default
    if (load_factor < MIN_LOAD_FACTOR) {
        printf("[WARN] %s found load_factor=%f < MIN_LOAD_FACTOR=%f, reset to "
               "MIN_LOAD_FACTOR\n",
               __func__, load_factor, MIN_LOAD_FACTOR);
        load_factor = MIN_LOAD_FACTOR;
    }

    if (load_factor > MAX_LOAD_FACTOR) {
        printf("[WARN] %s found load_factor=%f > MAX_LOAD_FACTOR=%f, reset to "
               "MAX_LOAD_FACTOR\n",
               __func__, load_factor, MAX_LOAD_FACTOR);
        load_factor = MAX_LOAD_FACTOR;
    }

    // Allocate the data array, initially zeroed using calloc
    new_ring_array->data = alloc_data(capacity, entry_size);
    if (new_ring_array->data == NULL) {
        free(new_ring_array);
        return NULL;
    }

    const size_t default_head_idx = default_head(capacity);
    new_ring_array->head = default_head_idx;
    new_ring_array->tail = default_head_idx;
    new_ring_array->length = 0;
    new_ring_array->is_packed = true;
    new_ring_array->_entry_size = entry_size;
    new_ring_array->_val_size = val_size;
    new_ring_array->_capacity = capacity;
    new_ring_array->_load_factor = load_factor;

    // Initialize the next & prev indexes on the head/tail entry
    set_idxs_on_entry(new_ring_array, new_ring_array->head,
                      RING_ARY_END, RING_ARY_BEGIN);

    return new_ring_array;
}

void ring_array_destroy(struct RingArray *const ring_array)
{
    if (ring_array != NULL) {
        free(ring_array->data);
        free(ring_array);
    }
};

bool ring_array_push_front(struct RingArray *const ring_array, const void *v)
{
    const size_t length = ring_array->length;
    size_t idx, next; // idx at which we will prepend the entry, and new next
    if (length == 0) { // No entries yet, prepend on head
        idx = ring_array->head;
        next = RING_ARY_END;
    } else { // Some entries, select head idx at which we'd like to prepend
        if (needs_resize(ring_array, 1)) {
            const size_t resize_start =
                    prepend_align_idx(length, 2 * ring_array->_capacity);
            const enum DefragRes res =
                    defragment(ring_array, resize_start, true);
            if (res != RE_INDEXED_AND_RESIZED) { // Both must succeed
                return false;
            }
        }

        size_t prev_head = ring_array->head;
        size_t capacity = ring_array->_capacity;
        idx = decr_idx(prev_head, 1, capacity);
        next = prev_head;

        if (idx == ring_array->tail) { // Array has wrapped around to tail
            const size_t resize_start =
                    prepend_align_idx(length, 2 * capacity);
            enum WrapHandle res = handle_wrapped(ring_array, resize_start);

            if (res == HANDLE_FAILED) {
                return false;
            } else if (res == RESIZED) { // Capacity changed, reload it
                capacity = ring_array->_capacity;
            } // other possibility is DEFRAGMENTED

            prev_head = ring_array->head;
            idx = decr_idx(prev_head, 1, capacity);
            next = prev_head;
        }
    }

    copy_val_to_entry(ring_array, idx, v);
    ring_array->head = idx;
    ring_array->length++;
    set_next_idx_on_entry(ring_array, idx, next);
    set_prev_idx_on_entry(ring_array, idx, RING_ARY_BEGIN);
    set_prev_idx_on_entry(ring_array, next, idx);

    return true;
}

bool ring_array_push_back(struct RingArray *const ring_array, const void *v)
{
    return true;
}

void ring_array_remove(struct RingArray *const ring_array, const size_t idx)
{

}

void ring_array_remove_range(struct RingArray *const ring_array,
                             const size_t idx, const size_t length)
{

}

void ring_array_clear(struct RingArray *const ring_array)
{

}

enum ResizeRes {
    RESIZE_FAILURE,
    SUCCESS
};

// Resizes the RingArray to the new capacity and re-refs to new_head
// Returns SUCCESS if the resize was successful, FAILURE otherwise
static enum ResizeRes ring_array_resize(struct RingArray *const ring_array,
                                        const size_t new_head)
{
    const size_t capacity = ring_array->_capacity;
    const size_t new_capacity = capacity * 2;
    if (new_capacity > MAX_CAPACITY) {
        printf("[ERROR] %s found new_capacity=%zu > MAX_CAPACITY, resize "
               "failed\n",
               __func__, new_capacity);
        return RESIZE_FAILURE;
    }
    assert(new_head + capacity < new_capacity);

    const size_t head = ring_array->head;
    const size_t tail = ring_array->tail;
    const size_t entries = idx_diff(head, tail, capacity);
    if (new_head + entries > new_capacity) {
        printf("[ERROR] %s found new_head=%zu + entries=%zu > new_capacity=%zu\n",
               __func__, new_head, entries, new_capacity);
        return RESIZE_FAILURE;
    }

    const size_t entry_size = ring_array->_entry_size;
    char *new_data = alloc_data(new_capacity, entry_size);
    if (new_data == NULL) {
        printf("[ERROR] %s failed to allocate new_data\n", __func__);
        return RESIZE_FAILURE;
    }

    char *const old_data = ring_array->data;
    size_t new_tail;
    if (head <= tail || tail == 0) {
        if (head != tail) {
            // Do a straightforward memcpy into the new array
            memcpy(new_data + new_head * entry_size, old_data + head * entry_size,
                   entries * entry_size);

            new_tail = new_head + entries;

            const size_t last_idx_to_update = (new_tail-1) & (new_capacity-1);
            char *const last_entry_to_update =
                    new_data + last_idx_to_update * entry_size;
            const size_t first_prev = RING_ARY_BEGIN;
            if (new_head != head) { // Update next/prev idxs on entries
                apply_idx_delta(new_data, last_entry_to_update, new_head,
                        entry_size, first_prev, (new_head - head));
            }
        } else {
            new_tail = new_head;
        }
    } else { /* old_data wrapped past end of array, split the copies */
        const size_t first_entries = capacity - head;
        memcpy(new_data + new_head * entry_size, old_data + head * entry_size,
               first_entries * entry_size);

        const size_t second_entries = tail;
        memcpy(new_data + (new_head + first_entries) * entry_size, old_data,
               second_entries * entry_size);

        assert(first_entries + second_entries == entries);

        new_tail = new_head + entries;
    }

    // Update the ring_array values
    ring_array->head = new_head;
    ring_array->tail = new_tail;
    ring_array->_capacity = new_capacity;

    // Swap in the new array and free the old one
    ring_array->data = new_data;
    free(old_data);

    return SUCCESS;
}

enum MemmoveRes {
    NEEDS_RESIZE,
    NO_MOVE_NEEDED,
    MOVED
};

// Attempts to copy n bytes from src position to dest position in data.
// Asks to resize the array in degenerate case where dest_end wraps past src
// Caller should reassign next and prev indices in each moved entry
static enum MemmoveRes memmove_range(char *data, size_t dest, size_t src,
                                     size_t n, const size_t elem_size,
                                     const size_t capacity)
{
    assert( n < capacity );

    if (dest == src) return NO_MOVE_NEEDED;

    bool needs_resize = false;

    if (src < dest) {
        const size_t dest_end = incr_idx(dest, n, capacity);
        if (dest < dest_end || dest_end == 0) { // 0..src..dest..dest_end][X
            memmove(data + dest*elem_size, data + src*elem_size, n*elem_size);
        } else { // dest_end wraps around array
            if (dest_end <= src) { // 0..dest_end][src..dest..X(capacity)
                const size_t src_end = incr_idx(src, n, capacity);
                if (src < src_end || src_end == 0) {
                    // 0..dest_end][src..dest..src_end][X(capacity)
                    size_t first_elems = dest_end;
                    size_t first_src = src_end == 0 ? capacity - first_elems
                                                      : src_end - first_elems;
                    // copy [first_src, capacity) to [0, dest_end)
                    memmove(data, data + first_src * elem_size,
                            first_elems * elem_size);
                    size_t second_elems = n - first_elems;
                    assert(second_elems < n);
                    // copy [src, first_src) to [dest, capacity)
                    memmove(data + dest * elem_size, data + src * elem_size,
                            second_elems * elem_size);
                } else { // 0..src_end..dest_end][src..dest..X(capacity)
                    size_t first_dest = dest_end - src_end;
                    // copy [0, src_end) to [first_dest, dest_end)
                    memmove(data + first_dest * elem_size, data,
                            src_end * elem_size);
                    size_t second_src = capacity - first_dest;
                    // copy [second_src, capacity) to [0, first_dest)
                    memmove(data, data + second_src * elem_size,
                            first_dest * elem_size);
                    size_t third_elems = n - first_dest - src_end;
                    assert(third_elems < n);
                    // copy [src, second_src) to [dest, capacity)
                    memmove(data + dest * elem_size, data + src * elem_size,
                            third_elems * elem_size);
                }
            } else { // degenerate case where dest_end wraps past src, resize
                needs_resize = true;
            }
        }
    } else { // dest < src
        const size_t src_end = incr_idx(src, n, capacity);
        if (src < src_end || src_end == 0) { // 0..dest..src..src_end][X
            memmove(data + dest*elem_size, data + src*elem_size, n*elem_size);
        } else { // src_end wraps around array
            if (src_end <= dest) { // 0..src_end][dest..src..X(capacity)
                const size_t dest_end = incr_idx(dest, n, capacity);
                const size_t first_elems = capacity - src;
                const size_t second_dest = dest + first_elems;
                if (dest < dest_end || dest_end == 0) {
                    // 0..src_end][dest..src..dest_end][X(capacity)
                    // copy [src, capacity) to [dest, second_dest)
                    memmove(data + dest * elem_size, data + src * elem_size,
                            first_elems * elem_size);
                    size_t second_elems = n - first_elems;
                    assert( second_elems < n );
                    // copy [0, src_end) to [second_dest, capacity)
                    memmove(data + second_dest * elem_size, data,
                            second_elems * elem_size);
                } else { // 0..dest_end..src_end][dest..src..x(capacity)
                    // copy [src, capacity) to [dest, second_dest)
                    memmove(data + dest * elem_size, data + src * elem_size,
                            first_elems * elem_size);
                    size_t second_elems = src - dest;
                    // copy [0, second_elems) to [second_dest, capacity)
                    memmove(data + second_dest * elem_size, data,
                            second_elems * elem_size);
                    size_t third_elems = n - first_elems - second_elems;
                    assert( third_elems < n );
                    // copy [second_elems, src_end) to [0, dest_end)
                    memmove(data, data + second_elems * elem_size,
                            third_elems * elem_size);
                }
            } else { // degenerate case where src_end wraps past dest, resize
                needs_resize = true;
            }
        }
    }

    return needs_resize ? NEEDS_RESIZE : MOVED;
}

// Reassigns all next and prev ptrs for a packed ring_array
static void reset_next_prev(struct RingArray *const ring_array)
{
    assert(ring_array->is_packed);

    const size_t capacity = ring_array->_capacity;

    size_t i = ring_array->head;
    const size_t tail = ring_array->tail;

    if (i == tail) {
        assert(ring_array->length == 0);
        set_next_idx_on_entry(ring_array, i, RING_ARY_END);
        set_prev_idx_on_entry(ring_array, i, RING_ARY_BEGIN);

        return;
    }

    size_t prev = RING_ARY_BEGIN;
    do {
        const size_t next_i = (i + 1) & (capacity - 1);
        set_idxs_on_entry(ring_array, i, next_i, prev);
        prev = i;
        i = next_i;
    } while (i != tail);

    // Set the next index of the last entry to RING_ARY_END
    set_next_idx_on_entry(ring_array, i, RING_ARY_END);
}

enum PackToHeadRes {
    RESIZE_FAILED,
    PACKED,
    PACKED_AND_RESIZED
};

static enum PackToHeadRes pack_to_head(struct RingArray *const ring_array,
                                       const size_t new_head_on_resize);

static enum PackToHeadRes
resize_and_pack_to_head(struct RingArray *const ring_array,
                        const size_t new_head)
{
    enum ResizeRes resizeRes =
            ring_array_resize(ring_array, new_head);

    if (resizeRes == RESIZE_FAILURE) return RESIZE_FAILED;

    enum PackToHeadRes packRes =
            pack_to_head(ring_array, new_head);
    return packRes != RESIZE_FAILED ? PACKED_AND_RESIZED : RESIZE_FAILED;
}

// Packs the entries in ring_array to ->head, OR resizes and then packs to
// new_head_on_resize if a resize turns out to be necessary
// NOTE: Does NOT reset next/prev indices since may be used in pack => move ops
static enum PackToHeadRes pack_to_head(struct RingArray *const ring_array,
                                       const size_t new_head_on_resize)
{
    assert(ring_array->length != 0);
    assert(!ring_array->is_packed);

    const size_t head = ring_array->head;

    const size_t capacity = ring_array->_capacity;
    assert( ring_array->tail < capacity );
    const size_t entry_size = ring_array->_entry_size;
    char *const data = ring_array->data;

    // Align contiguous blocks of active cells to current head
    size_t to_start = head;
    size_t from_start = to_start;
    size_t from_end = to_start;

    for (;;) {
        char *const entry = data + from_end * entry_size;
        const size_t next = get_next_idx_on_entry(entry);
        const size_t contig_next =
                from_end + 1 == capacity ? 0 : from_end + 1;

        if (next == RING_ARY_END) {
            from_end = contig_next;
            const size_t vals = idx_diff(from_start, from_end, capacity);
            assert(vals > 0); // end should be found by active entry

            if (to_start != from_start) { // Copy last active region
                enum MemmoveRes res = memmove_range(data, to_start, from_start,
                                                    vals, entry_size, capacity);
                if (res == NEEDS_RESIZE) {
                    // Recursively resizes the array and calls pack
                    return resize_and_pack_to_head(ring_array,
                                                   new_head_on_resize);
                }
            }

            // Move to_start to mark new end index of list
            to_start = incr_idx(to_start, vals, capacity);

            break;
        } else {
            if (next == contig_next) { // Nothing to do, incr from_end
                from_end = next;
            } else { // Break in active cells, re-pack the region
                const size_t vals =
                        idx_diff(from_start, contig_next, capacity);
                if (to_start != from_start) { // Move active region left
                    enum MemmoveRes res =
                            memmove_range(data, to_start, from_start, vals,
                                    entry_size, capacity);
                    if (res == NEEDS_RESIZE) {
                        // Recursively resizes the array and calls pack
                        return resize_and_pack_to_head(ring_array,
                                                       new_head_on_resize);
                    }
                }

                to_start = contig_next;
                from_start = next;
                from_end = next;
            }
        }
    } // On loop exit, to_start == the end point of the active cell region

    // Reset the tail, next/prev ptrs on entries, and is_packed
    ring_array->tail = to_start;

    assert(incr_idx(ring_array->head, ring_array->length, ring_array->_capacity)
               == ring_array->tail);

    ring_array->is_packed = true;

    return PACKED;
}

// Defragments the array by:
// - Copying all active cells to a contiguous region starting at ->head,
// - Copying that active region to begin at start_idx, resizing the backing
//   data array if necessary.
static enum DefragRes defragment(struct RingArray *const ring_array,
                                 const size_t start_idx,
                                 const bool force_resize)
{
    const size_t length = ring_array->length;

    if (ring_array->head == start_idx && ring_array->is_packed) {
        if (!force_resize) {
            return RE_INDEXED;
        } else {
            enum ResizeRes res = ring_array_resize(ring_array, start_idx);
            // No changes to next/prev should be necessary w head idx match
            return res == RESIZE_FAILURE ? DEFRAG_FAILED
                                         : RE_INDEXED_AND_RESIZED;
        }
    }

    if (length == 0) { // Simply update the head and tail indices
        ring_array->head = start_idx;
        ring_array->tail = start_idx;
        ring_array->is_packed = true;
        enum DefragRes res = RE_INDEXED;
        if (force_resize) {
            enum ResizeRes resize_res =
                    ring_array_resize(ring_array, start_idx);
            res = resize_res == RESIZE_FAILURE ? DEFRAG_FAILED
                                               : RE_INDEXED_AND_RESIZED;
        }
        reset_next_prev(ring_array); // Handles length==0 case automatically
        return res;
    }

    assert(ring_array->tail < ring_array->_capacity);

    char *const data = ring_array->data;
    const size_t elem_size = ring_array->_entry_size;

    // Align contiguous blocks of active cells to current head
    enum PackToHeadRes packRes;
    if (force_resize) {
        packRes = resize_and_pack_to_head(ring_array, start_idx);
    } else {
        packRes = pack_to_head(ring_array, start_idx);
    }

    if (packRes == RESIZE_FAILED) {
        return DEFRAG_FAILED;
    }

    if (packRes == PACKED_AND_RESIZED) {
        assert(ring_array->head == start_idx);
        reset_next_prev(ring_array);
        return RE_INDEXED_AND_RESIZED;
    }

    // If we reach this point, did not complete a resize in pack call above
    const size_t head = ring_array->head;
    assert(packRes == PACKED);
    if (head == start_idx) {
        // Have already packed to desired start_idx, can reset idxs and return
        reset_next_prev(ring_array);
        return RE_INDEXED;
    }

    const size_t capacity = ring_array->_capacity;
    // Cells have been made contiguous but not yet correctly re-indexed
    // Old reference to data pointer still valid since there was no resize
    enum MemmoveRes moveRes =
            memmove_range(data, start_idx, head, length, elem_size, capacity);

    if (moveRes != NEEDS_RESIZE) {
        if (moveRes == MOVED) {
            ring_array->head = start_idx;
            ring_array->tail = incr_idx(start_idx, length, capacity);
        } else {
            assert(moveRes == NO_MOVE_NEEDED);
        }
        reset_next_prev(ring_array);
        return RE_INDEXED;
    }

    // Needed resize to complete move
    assert(moveRes == NEEDS_RESIZE);
    enum ResizeRes resizeRes = ring_array_resize(ring_array, start_idx);
    if (resizeRes == SUCCESS) {
        reset_next_prev(ring_array);
        return RE_INDEXED_AND_RESIZED;
    } else {
        return DEFRAG_FAILED;
    }
}

static enum WrapHandle handle_wrapped(struct RingArray *const ring_array,
                                      const size_t start_idx)
{
    assert((float)(ring_array->length + 1) / (float)ring_array->_capacity <
           ring_array->_load_factor);

    // Defragment the array without forcing a resize
    enum DefragRes defrag_res = defragment(ring_array, start_idx, false);
    switch (defrag_res) {
        case DEFRAG_FAILED:
            return HANDLE_FAILED;
        case RE_INDEXED:
            return DEFRAGMENTED;
        case RE_INDEXED_AND_RESIZED:
            return RESIZED;
        default:
            printf("[ERROR] %s had unexpected defrag_res=%d\n", __func__,
                   defrag_res);
            return HANDLE_FAILED;
    }
}

static inline char *alloc_data(size_t capacity, size_t entry_size)
{
    // Allocate the data array, initially zeroed using calloc
    char *data = (char *)calloc(capacity, entry_size);

    if (data == NULL) return NULL;

    return data;
}

