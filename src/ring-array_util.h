#ifndef RINGARRAY_UTIL_H
#define RINGARRAY_UTIL_H

#include <stddef.h>

extern size_t next_power_of_2(size_t);

#endif
