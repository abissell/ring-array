#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include <assert.h>

#include "ring-array_util.h"

START_TEST (test_print)
{
    printf("test\n");
}
END_TEST

START_TEST (test_next_power_of_2)
{
    size_t next_2 = next_power_of_2((size_t) 2);
    assert(next_2 == 2);

    size_t next_3 = next_power_of_2((size_t) 3);
    assert(next_3 == 4);

    size_t next_7 = next_power_of_2((size_t) 7);
    assert(next_7 == 8);
}
END_TEST

Suite *instr_suite(void)
{
    Suite *s = suite_create ("ring-array");

    TCase *tc_core = tcase_create("core");
    tcase_add_test(tc_core, test_print);
    tcase_add_test(tc_core, test_next_power_of_2);
    suite_add_tcase(s, tc_core);

    return s;
}

int main()
{
    int num_failed;
    Suite *s = instr_suite();
    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_NORMAL);
    num_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (num_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
